import colors from 'vuetify/es5/util/colors'
require('dotenv').config({ path: process.env.ENV_FILE_PATH })

export default {
  serverMiddleware: [
    { path: '/interface_data/blog', handler: '~/middleware/blog.js' },
    {
      path: '/interface_data/blast_params',
      handler: '~/middleware/blastParams.js',
    },
    {
      path: '/interface_data/target_predictions',
      handler: '~/middleware/targetPredictions.js',
    },
  ],
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Base URL
   */
  router: {
    base: process.env.BASE_PATH || '/chembl',
  },
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    basePath: process.env.BASE_PATH || '/chembl',
    delayedJobsBaseUrl:
      process.env.DELAYED_JOBS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/interface_api/delayed_jobs',
    esProxyBaseUrl:
      process.env.ES_PROXY_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/interface_api/es_proxy',
    unichemAPIBaseUrl:
      process.env.UNICHEM_API_BASE_URL ||
      'https://www.ebi.ac.uk/unichem/api/v1',
    unichemWebBaseURL:
      process.env.UNICHEM_WEB_BASE_URL || 'https://www.ebi.ac.uk/unichem',
    delayedJobsIgnoreCache: process.env.DELAYED_JOBS_IGNORE_CACHE === 'true',
    gladosBackboneBaseUrl:
      // process.env.GLADOS_BACKBONE_BASE_URL || 'http://0.0.0.0:8000',
      process.env.GLADOS_BACKBONE_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/old',
    iframeTargetOrigin:
      process.env.IFRAME_TARGET_ORIGIN || 'http://0.0.0.0:8000',
    esIndexPrefix: process.env.ES_INDEX_PREFIX || 'chembl_',
    compoundReportCardsBaseUrl:
      process.env.COMPOUND_REPORT_CARDS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/explore/compound',
    assayReportCardsBaseUrl:
      process.env.ASSAY_REPORT_CARDS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/explore/assay',
    documentReportCardsBaseUrl:
      process.env.DOCUMENT_REPORT_CARDS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/explore/document',
    targetReportCardsBaseUrl:
      process.env.TARGET_REPORT_CARDS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/explore/target',
    cellLineReportCardsBaseUrl:
      process.env.CELL_LINE_REPORT_CARDS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/explore/cell_line',
    tissueReportCardsBaseUrl:
      process.env.TISSUE_REPORT_CARDS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/explore/tissue',
    djJobStatusCheckIntervalMillis: process.env
      .DJ_JOB_STATUS_CHECK_INTERVAL_MILLIS
      ? parseInt(process.env.DJ_JOB_STATUS_CHECK_INTERVAL_MILLIS)
      : 2000,
    entityJoinDestinationTemplate:
      process.env.ENTITY_JOIN_DESTINATION_TEMPLATE ||
      'wwwdev.ebi.ac.uk/chembl/explore/<BROWSER_NAME>/full_state/<GENERATED_STATE>',
    similaritySearchUrlTemplate:
      process.env.SIMILARITY_SEARCH_URL_TEMPLATE ||
      'http://0.0.0.0:3000/chembl/advanced_search/similarity/<TERM>/<THRESHOLD>',
    connectivitySearchUrlTemplate:
      process.env.CONNECTIVITY_SEARCH_URL_TEMPLATE ||
      'http://0.0.0.0:3000/chembl/advanced_search/connectivity/<TERM>',
    substructureSearchUrlTemplate:
      process.env.SUBSTRUCTURE_SEARCH_URL_TEMPLATE ||
      'http://0.0.0.0:3000/chembl/advanced_search/substructure/<TERM>',
    searchByIDsUrlTemplate:
      process.env.SEARCH_BY_IDS_TEMPLATE ||
      'http://0.0.0.0:3000/chembl/advanced_search/search_by_ids/<JOB_ID>',
    entityBrowserStateUrlTemplateNew:
      process.env.ENTITY_BROWSER_STATE_URL_TEMPLATE_NEW ||
      'http://0.0.0.0:3000/chembl/explore/<BROWSER_NAME>/<GENERATED_STATE>',
    heatmapUrlTemplate:
      process.env.HEATMAP_URL_TEMPLATE ||
      'https://wwwdev.ebi.ac.uk/chembl/heatmap/HEATMAP_ID:<HEATMAP_ID>',
    hastagUrlsBase:
      process.env.HASHTAG_URLS_BASE || 'https://wwwdev.ebi.ac.uk/chembl/g/',
    wsLookupBaseUrl:
      process.env.WS_LOOKUP_BASE_URL || 'https://wwwdev.ebi.ac.uk',
    chemblWSBaseUrl:
      process.env.CHEMBL_WS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/api/data',
    fallbackIMGsBaseUrl:
      process.env.FALLBACK_IMGS_BASE_URL ||
      'https://www.ebi.ac.uk/chembl/k8s/static/chembl/img',
    metadataChEMBLDescription:
      process.env.METADATA_CHEMBL_DESCRIPTION ||
      'ChEMBL is a manually curated database of bioactive molecules with drug-like properties.',
    canonicalDomain: process.env.CANONICAL_DOMAIN || 'www.ebi.ac.uk',
    beakerBaseURL:
      process.env.BEAKER_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/api/utils',
    freeTextsearchesURLTemplate:
      process.env.FREE_TEXT_SEARCHES_URL_TEMPLATE ||
      'http://0.0.0.0:3000/chembl/search_results/<TERM>',
    freeTextsearchesWithStateURLTemplate:
      process.env.FREE_TEXT_SEARCHES_WITH_STATE_URL_TEMPLATE ||
      'http://0.0.0.0:3000/chembl/search_results/<TERM>/<STATE_ID>',
    blastSearchUrlTemplate:
      process.env.BLAST_SEARCH_URL_TEMPLATE ||
      'http://0.0.0.0:3000/chembl/advanced_search/blast/<DESCRIPTOR>',
    searchByIDsResultsURLTemplate:
      process.env.SEARCH_BY_IDS_RESULTS_URL_TEMPLATE ||
      'http://0.0.0.0:3000/chembl/advanced_search/search_by_ids/<JOB_ID>',
    esDataMaxURLLength: process.env.ES_DATA_MAX_URL_LENGTH || 2048,
    chemblFTPBaseURL:
      process.env.CHEMBL_FTP_BASE_URL ||
      'https://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest',
    faviconPath: process.env.FAVICON_PATH || '/A-favicon.ico',
    // 'marvinjs' or 'ketcher'
    chemicalEditor: process.env.CHEMICAL_EDITOR || 'marvinjs',
    // set to 'http://0.0.0.0:3000/chembl/ketcher-services/index.html' for local development with ketcher
    editorBaseURL:
      process.env.EDITOR_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/marvinjs-services/',
    bloggerKey: process.env.BLOGGER_KEY,
  },
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    titleTemplate: '%s - ChEMBL',
    title: 'ChEMBL' || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        // In production, this should be set to '/chembl/favicon.ico' so browsers display it correctly
        href: process.env.faviconPath || '/chembl/favicon.ico',
      },
    ],
  },
  /*
   ** Global CSS
   */
  css: ['vue-resize/dist/vue-resize.css'],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [{ src: '~/plugins/apexcharts.js', mode: 'client' }],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [],
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    treeShake: true,
    customVariables: ['~/assets/variables.scss'],
    theme: {
      themes: {
        light: {
          primary: '#09979b',
          accent: '#00bfa5',
          secondary: '#62b7bd',
          info: colors.blue.darken2,
          warning: '#6B5000',
          error: colors.deepOrange.accent4,
          success: colors.green.darken3,
        },
      },
    },
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    // extend(config, { isDev, isClient }) {
    //   config.output.hashFunction = 'xxhash64'
    //   // config.experiments = {
    //   //   futureDefaults: true,
    //   // }
    // },
  },
}
