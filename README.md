# ChEMBL Main Web Interface

This project contains the code for the main web interface.

https://www.ebi.ac.uk/chembl/

*important:* The folder [web-components-submodule](https://gitlab.ebi.ac.uk/chembl/chembl/main-web-interface/web-components-submodule) is a git submodule, make sure to include it when you clonde the repository:

```
git clone --recursive [URL to Git repo]
```

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

