const axios = require('axios')
const cache = require('memory-cache')

const BLOG_ID = '2546008714740235720'
const API_KEY = process.env.BLOGGER_KEY
const CACHE_TIME = 1800 * 1000 // Cache for 30 minutes

module.exports = async (req, res) => {
  let pageToken = (req.query && req.query.pageToken) || null

  // fallback if not able to get pageToken from query
  const regex = /[?&]pageToken=([^&]+)/
  const match = req.url.match(regex)
  if (match) {
    pageToken = match[1]
  }

  const cacheKey = pageToken || 'initial'

  const cachedResponse = cache.get(cacheKey)
  if (cachedResponse) {
    // Set response headers and return the cached JSON
    res.setHeader('Content-Type', 'application/json')
    return res.end(JSON.stringify(cachedResponse))
  }

  try {
    const response = await axios.get(
      `https://www.googleapis.com/blogger/v3/blogs/${BLOG_ID}/posts`,
      {
        params: {
          key: API_KEY,
          maxResults: 12,
          orderBy: 'updated',
          pageToken,
          fetchBodies: true,
          fetchImages: false,
        },
      }
    )

    const blogResponse = await axios.get(
      `https://www.googleapis.com/blogger/v3/blogs/${BLOG_ID}`,
      {
        params: { key: API_KEY },
      }
    )

    const totalCount = blogResponse.data.posts.totalItems
    const nextPageToken = response.data.nextPageToken
    const blogEntries = response.data.items.map((entry) => {
      const publishedDate = entry.published.split('T')[0]
      const updatedDate = entry.updated.split('T')[0]
      return {
        title: entry.title,
        url: entry.url,
        author: entry.author.displayName,
        author_url: entry.author.url,
        published_date: publishedDate,
        updated_date: updatedDate,
        content: entry.content,
      }
    })

    const responseData = {
      entries: blogEntries,
      nextPageToken,
      totalCount,
    }

    cache.put(cacheKey, responseData, CACHE_TIME) // Cache the response

    // Set response headers and return JSON response
    res.setHeader('Content-Type', 'application/json')
    return res.end(JSON.stringify(responseData))
  } catch (error) {
    // Return error response
    res.statusCode = 500
    res.setHeader('Content-Type', 'application/json')
    console.error('Failed to fetch blog entries:', error)
    return res.end(JSON.stringify({ error: 'Failed to fetch blog entries.' }))
  }
}
