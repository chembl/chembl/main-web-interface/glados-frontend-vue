const axios = require('axios')

// Define constants
const BLAST_API_BASE_URL = 'https://www.ebi.ac.uk/Tools/services/rest/ncbiblast'
const PARAMS_SEARCH_LINKS = {
  matrix:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-matrix',
  gapopen:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-gapopen',
  gapext:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-gapext',
  exp:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-exp',
  filter:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-filter',
  dropoff:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-dropoff',
  scores:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-scores',
  alignments:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-alignments',
  seqrange:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-seqrange',
  gapalign:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-gapalign',
  align:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-align',
  compstats:
    'https://www.ebi.ac.uk/seqdb/confluence/pages/viewpage.action?pageId=68167377#NCBIBLAST+(ProteinDatabases)-compstats',
}

// Handler function for fetching BLAST parameters
module.exports = async (req, res) => {
  try {
    // Request parameter names
    const paramsUrl = `${BLAST_API_BASE_URL}/parameters`
    const paramsResponse = await axios.get(paramsUrl)
    const paramsResult = paramsResponse.data

    // Extract parameter names and filter them
    const allParamNames = paramsResult.parameters
    const adjustableParams = allParamNames.filter(
      (p) =>
        ![
          'program',
          'task',
          'match_scores',
          'stype',
          'database',
          'transltable',
          'taxids',
          'negative_taxids',
        ].includes(p)
    )

    const finalParamsList = await Promise.all(
      adjustableParams.map(async (paramId) => {
        const paramDetailsUrl = `${BLAST_API_BASE_URL}/parameterdetails/${paramId}`
        const paramDetailsResponse = await axios.get(paramDetailsUrl)
        const paramRoot = paramDetailsResponse.data
        const param = {
          param_id: paramId,
          param_name: paramRoot.name,
          param_description: paramRoot.description,
          param_type: paramRoot.type,
          param_values: [],
          allow_free_input: ['seqrange', 'sequence'].includes(paramId),
          param_help_link: PARAMS_SEARCH_LINKS[paramId] || null,
        }

        const possibleValues =
          paramRoot.values != null ? paramRoot.values.values : null

        if (possibleValues != null) {
          param.param_values = possibleValues.map((valueDesc) => {
            return {
              label: valueDesc.label,
              value: valueDesc.value,
              is_default: valueDesc.defaultValue,
            }
          })

          // Set default value if allow_free_input
          if (param.allow_free_input) {
            const defaultValue = param.param_values.find((v) => v.is_default)
              ?.value
            if (defaultValue) param.default_value = defaultValue
          }
        }

        return param
      })
    )

    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({ params: finalParamsList }))
  } catch (error) {
    console.error('Error fetching BLAST parameters:', error)
    res.statusCode = 500
    res.end(JSON.stringify({ error: 'Failed to fetch BLAST parameters' }))
  }
}
