// this middleware proxies the target predictions endpoint to make things easier about CORS and other issues
const axios = require('axios')
const cache = require('memory-cache')
const CACHE_TIME = 300 * 1000 // Cache for 5 minutes

module.exports = async (req, res) => {
  try {
    // remove initial slash
    const rawSmiles = req.url.slice(1)
    const smiles = decodeURIComponent(rawSmiles).replace(/__SLASH__/g, '/')

    const cacheKey = `target_predictions_${smiles}`
    const cachedResponse = cache.get(cacheKey)
    if (cachedResponse) {
      // Set response headers and return the cached JSON
      res.setHeader('Content-Type', 'application/json')
      return res.end(JSON.stringify(cachedResponse))
    }

    const targetPredictionsResponse = await axios.post(
      'https://www.ebi.ac.uk/chembl/target-predictions',
      JSON.stringify({ smiles }),
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }
    )
    const responseData = targetPredictionsResponse.data

    cache.put(cacheKey, responseData, CACHE_TIME) // Cache the response

    // Set response headers and return JSON response
    res.setHeader('Content-Type', 'application/json')
    return res.end(JSON.stringify(responseData))
  } catch (error) {
    // Return error response
    res.statusCode = 500
    res.setHeader('Content-Type', 'application/json')
    console.error('Failed to fetch target predictions:', error)
    return res.end(
      JSON.stringify({ error: 'Failed to fetch target predictions.' })
    )
  }
}
