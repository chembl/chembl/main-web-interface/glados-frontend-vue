<template>
  <div
    class="landing-page-histogram-container"
    :class="landingPageHistogramClasses"
  >
    <v-card flat height="100%">
      <v-card-text v-if="initialLoad">
        <v-skeleton-loader type="image" width="100%" height="100%" />
      </v-card-text>
      <v-card-text v-else-if="showError" class="text-caption error--text">
        Error: {{ errorMsg }}.
      </v-card-text>
      <v-card-text v-else-if="noDataAvailable">
        <div class="d-flex justify-center">No data available.</div>
      </v-card-text>
      <v-card-text v-else>
        <template v-if="showHistogramSettings">
          <HistogramSettings
            :current-interval="currentInterval"
            :min-interval="minInterval"
            :max-interval="maxInterval"
            :loading-histogram-data="loadingHistogramData"
            :disable-less-bars="disableLessBars"
            :disable-more-bars="disableMoreBars"
            @increase-interval="increaseInterval"
            @decrease-interval="decreaseInterval"
            @update-interval="updateInterval"
          />
          <br />
          <v-divider />
        </template>
        <div v-if="loadingHistogramData" class="d-flex justify-center">
          <v-progress-circular color="primary" :size="50" indeterminate />
        </div>
        <div
          v-else-if="showHistogramDataError"
          class="text-caption error--text"
        >
          Error: {{ histogramErrorMsg }}.
        </div>
        <div v-else class="histogram-container">
          <StackedHistogram
            :histogram-data="histogramData"
            :histogram-height="inCarousel ? '60%' : '50%'"
            :legend-on-top="inCarousel"
            percentage-scale
          />
        </div>
      </v-card-text>
    </v-card>
  </div>
</template>

<script>
import { HistogramMixin } from '~/web-components-submodule/mixins/Histograms/HistogramMixin.js'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import IndexNames from '~/web-components-submodule/standardisation/IndexNames.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import LinksToBrowsers from '~/web-components-submodule/standardisation/LinksToBrowsers.js'
import StackedHistogram from '~/components/report_cards/shared/Histogram/StackedHistogram.vue'
import HistogramSettings from '~/components/report_cards/shared/Histogram/HistogramSettings.vue'

export default {
  components: { StackedHistogram, HistogramSettings },
  mixins: [HistogramMixin],
  props: {
    inCarousel: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      entityID: EntityNames.Document.entityID,
      categoriesField: 'first_approval',
      xPropertyLabel: 'Year',
      subcategoriesLabel: 'Molecule Type',
      // the interval definition in this case is customised because we are dealing with years
      minInterval: 1,
      currentInterval: 1,
      intervalStep: 1,
      loadingHistogramData: false,
    }
  },
  computed: {
    showHistogramSettings() {
      return !this.inCarousel
    },
    landingPageHistogramClasses() {
      return {
        'in-carousel': this.inCarousel,
      }
    },
    // since the index for drugs is chembl_molecule, we need to add the query to filter only drugs
    allDataQuerystring() {
      return '_metadata.drug.is_drug:true AND _exists_:first_approval'
    },
    aggQuery() {
      return {
        size: 0,
        query: {
          query_string: {
            query: this.allDataQuerystring,
          },
        },
        aggs: {
          years: {
            histogram: {
              field: this.categoriesField,
              interval: this.currentInterval,
            },
            aggs: {
              types: {
                terms: {
                  field: 'molecule_type',
                  size: 20,
                  missing: this.missingLabel,
                },
              },
            },
          },
        },
      }
    },
  },
  methods: {
    async loadInitialDetails() {
      try {
        const drugsIndexName = IndexNames.getIndexNameFromEntityID(
          EntityNames.Drug.entityID
        )

        const countResponse = await ESProxyService.getESData(
          drugsIndexName,
          this.countQuery
        )
        this.numItems = countResponse.data.es_response.hits.total.value

        const minMaxResponse = await ESProxyService.getESData(
          drugsIndexName,
          this.minMaxQuery
        )

        this.minXValue =
          minMaxResponse.data.es_response.aggregations[this.minAggName].value
        this.maxXValue =
          minMaxResponse.data.es_response.aggregations[this.maxAggName].value

        this.calculateInitialInterval()
        this.initialLoad = false
        this.loadHistogramData()
      } catch (error) {
        this.loadingHistogramData = false
        this.showHistogramDataError = true
        this.histogramErrorMsg = error
      }
    },
    calculateInitialInterval() {
      const numYears = this.maxXValue - this.minXValue
      const minBins = 2
      this.maxInterval = Math.max(Math.ceil(numYears / minBins), 1)

      let numDesiredBins

      if (this.$vuetify.breakpoint.lg || this.$vuetify.breakpoint.xl) {
        numDesiredBins = this.inCarousel ? 6 : 12
      } else if (this.$vuetify.breakpoint.md) {
        numDesiredBins = this.inCarousel ? 5 : 10
      } else if (this.$vuetify.breakpoint.sm) {
        numDesiredBins = this.inCarousel ? 4 : 8
      } else {
        numDesiredBins = this.inCarousel ? 3 : 6
      }

      const desiredInterval = Math.ceil(numYears / numDesiredBins)
      this.currentInterval = Math.max(
        Math.min(this.maxInterval, desiredInterval),
        this.minInterval
      )
    },
    parseStackedHistogramData(buckets) {
      const categories = []

      for (let i = 0; i < buckets.length; i++) {
        const bucket = buckets[i]
        const currentYear = bucket.key

        let label
        let querystring
        let datasetDescriptionText
        if (this.currentInterval === 1) {
          label = currentYear
          querystring = `first_approval:(${currentYear})`
          datasetDescriptionText = `Drugs Approved in ${currentYear}`
        } else {
          const nextYear =
            i + 1 < buckets.length ? buckets[i + 1].key : this.maxXValue
          label =
            i + 1 < buckets.length
              ? `[${currentYear}, ${nextYear})`
              : `[${currentYear}, ${nextYear}]`

          const valueRange =
            i + 1 < buckets.length
              ? `[${currentYear} TO ${nextYear}}`
              : `[${currentYear} TO ${nextYear}]`

          querystring = `first_approval:${valueRange}`
          datasetDescriptionText = `Drugs Approved from ${currentYear} to before ${nextYear}`
        }

        const link = LinksToBrowsers.buildURLForEntityBrowser(
          EntityNames.Drug.entityID,
          querystring,
          'querystring',
          datasetDescriptionText
        )

        const category = {
          label,
          position: i,
          count: bucket.doc_count,
          querystring,
          datasetDescriptionText,
          link,
          categories: this.parseSubCategories(
            bucket.types.buckets,
            querystring,
            datasetDescriptionText,
            bucket.types.sum_other_doc_count
          ),
        }
        categories.push(category)
      }

      const allTypesData = categories.map((c) => c.categories).flat()
      const containsMissing = allTypesData.some(
        (molType) => molType.label === this.missingLabel
      )

      const legend = allTypesData.reduce((acc, molType) => {
        const label = molType.label
        const currentCount = molType.count

        if (acc[label] != null) {
          acc[label].count += currentCount
          return acc
        }

        let datasetDescriptionText
        let querystring

        if (label === this.missingLabel) {
          datasetDescriptionText = `Approved drugs with no molecule type.`
          querystring = `NOT _exists_:molecule_type AND _exists_:first_approval`
        } else {
          datasetDescriptionText = `Approved drugs of type ${label}`
          querystring = `molecule_type:"${label}" AND _exists_:first_approval`
        }

        if (label === this.otherLabel) {
          querystring = `NOT molecule_type:(${allTypesData
            .filter((j) => j.label !== this.missingLabel)
            .map((j) => `"${j.label}"`)
            .join(' OR ')})`

          if (containsMissing) {
            querystring = `${querystring} AND _exists_:molecule_type AND _exists_:first_approval`
          }
        }

        const link = LinksToBrowsers.buildURLForEntityBrowser(
          EntityNames.Drug.entityID,
          querystring,
          'querystring',
          datasetDescriptionText
        )
        acc[label] = { label, link, count: currentCount }
        return acc
      }, {})

      return { categories, legend }
    },
    parseSubCategories(
      buckets,
      parentQuerystring,
      parentDatasetDescriptionText,
      sumOtherDocCount
    ) {
      const subCategories = []

      for (let i = 0; i < buckets.length; i++) {
        const bucket = buckets[i]
        let datasetDescriptionText
        let querystring
        if (bucket.key === this.missingLabel) {
          datasetDescriptionText = `${parentDatasetDescriptionText} with no type`
          querystring = `${parentQuerystring} AND NOT _exists_:molecule_type`
        } else {
          datasetDescriptionText = `${parentDatasetDescriptionText} of type ${bucket.key}`
          querystring = `${parentQuerystring} AND molecule_type:"${bucket.key}"`
        }

        const link = LinksToBrowsers.buildURLForEntityBrowser(
          EntityNames.Drug.entityID,
          querystring,
          'querystring',
          datasetDescriptionText
        )
        const subCategory = {
          label: bucket.key,
          count: bucket.doc_count,
          querystring,
          datasetDescriptionText,
          link,
        }
        subCategories.push(subCategory)
      }

      if (sumOtherDocCount > 0) {
        const datasetDescriptionText = `${parentDatasetDescriptionText} from other types`
        let querystring = `${parentQuerystring} AND NOT molecule_type:(${buckets
          .filter((b) => b.key !== this.missingLabel)
          .map((b) => `"${b.key}"`)
          .join(' OR ')})`

        const hasMissing = buckets.some((b) => b.key === this.missingLabel)

        if (hasMissing) {
          querystring = `${querystring} AND _exists_:molecule_type`
        }

        const link = LinksToBrowsers.buildURLForEntityBrowser(
          EntityNames.Document.entityID,
          querystring,
          'querystring',
          datasetDescriptionText
        )
        const subCategory = {
          label: this.otherLabel,
          count: sumOtherDocCount,
          querystring,
          datasetDescriptionText,
          link,
        }

        subCategories.push(subCategory)
      }

      // sort the subcategories by label
      subCategories.sort((a, b) => -a.label.localeCompare(b.label))

      return subCategories
    },
    async loadHistogramData() {
      this.loadingHistogramData = true

      const drugsIndexName = IndexNames.getIndexNameFromEntityID(
        EntityNames.Drug.entityID
      )

      try {
        const histogramResponse = await ESProxyService.getESData(
          drugsIndexName,
          this.aggQuery
        )

        const buckets =
          histogramResponse.data.es_response.aggregations.years.buckets

        this.histogramData = {
          ...this.parseStackedHistogramData(buckets),

          xPropertyLabel: this.xPropertyLabel,
          subcategoriesLabel: this.subcategoriesLabel,
        }

        const itemsWithNoCategoryResponse = await ESProxyService.getESData(
          drugsIndexName,
          this.noCategoryQuery
        )

        this.numItemsWithNoCategory =
          itemsWithNoCategoryResponse.data.es_response.hits.total.value

        this.loadingHistogramData = false
      } catch (error) {
        this.loadingHistogramData = false
        this.showHistogramDataError = true
        this.histogramErrorMsg = error
      }
    },
  },
}
</script>

<style scoped lang="scss">
.landing-page-histogram-container {
  height: 100vh;
  &.in-carousel {
    height: 100%;
  }
}
.histogram-container {
  width: 100%;
}
</style>
