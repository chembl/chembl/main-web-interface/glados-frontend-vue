export const separatorPropertiesMixin = {
  props: {
    separator: {
      type: Object,
      default: () => {},
    },
    separators: {
      type: Array,
      default: () => [],
    },
    automaticSeparatorLoaded: {
      type: Boolean,
      default: () => undefined,
    },
    identifiedSeparatorCharacter: {
      type: String,
      default: () => undefined,
    },
  },
  methods: {
    emitSeparatorChanged(newSeparator) {
      this.$emit('SEPARATOR_CHANGED', newSeparator)
    },
  },
}
