export const state = () => ({
  openDialog: false,
  drawnMolecule: undefined,
})

export const mutations = {
  SET_OPEN_DIALOG(state, openDialog) {
    state.openDialog = openDialog
  },
  SET_DRAWN_MOLECULE(state, drawnMolecule) {
    state.drawnMolecule = drawnMolecule
  },
}

export const actions = {
  updateOpenDialog({ commit, state }, openDialog) {
    commit('SET_OPEN_DIALOG', openDialog)
  },
  setDrawnMolecule({ commit, state }, drawnMolecule) {
    commit('SET_DRAWN_MOLECULE', drawnMolecule)
  },
}
