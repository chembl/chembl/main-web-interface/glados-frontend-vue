import * as d3 from 'd3'

const getSunburstColorInterpolator = () => {
  return d3.interpolateHcl('#009688', '#9C27B0')
}

export default {
  getSunburstColorInterpolator,
}
