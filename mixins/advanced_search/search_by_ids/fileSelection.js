export const fileSelectionMixin = {
  methods: {
    emitFileSelected(idsFile) {
      this.$emit('FILE_SELECTED', idsFile)
    },
  },
}
