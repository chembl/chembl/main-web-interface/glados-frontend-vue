import { djApiClient } from '~/web-components-submodule/api_clients/DelajedJobsAPIClient.js'

export default {
  getSearchJobStatus(jobID) {
    return djApiClient.get(`/status/${jobID}`)
  },
  getSearchSummary(summaryUrl) {
    return djApiClient.get(summaryUrl)
  },
}
