import { djApiClient } from '~/web-components-submodule/api_clients/DelajedJobsAPIClient.js'

export default {
  getSearchJobStatusUrl(jobID) {
    return `${djApiClient.defaults.baseURL}/status/${jobID}`
  },
}
