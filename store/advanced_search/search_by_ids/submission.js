import SubmissionService from '@/services/advanced_search/search_by_ids/SubmissionService.js'
import Common from '@/services/advanced_search/search_by_ids/Common.js'
import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'

const _ = require('lodash/fp/object')

const fromToMap = {
  f1: ['t1'],
  f2: ['t2'],
}

const possibleFroms = {
  f1: {
    id: 'f1',
    name: 'Molecule ChEMBL IDs',
    hint: 'Provide Molecule ChEMBL IDs below',
    value: 'MOLECULE_CHEMBL_IDS',
  },
  f2: {
    id: 'f2',
    name: 'Target ChEMBL IDs',
    hint: 'Provide Target ChEMBL IDs below',
    value: 'TARGET_CHEMBL_IDS',
  },
}

const possibleTos = {
  t1: {
    id: 't1',
    name: 'ChEMBL Molecules',
    hint: 'Get ChEMBL Molecules',
    value: 'CHEMBL_COMPOUNDS',
  },
  t2: {
    id: 't2',
    name: 'ChEMBL Targets',
    hint: 'Get ChEMBL Targets',
    value: 'CHEMBL_TARGETS',
  },
}

const examplesMap = {
  f1: {
    t1: [
      'CHEMBL1905569',
      'CHEMBL3659411',
      'CHEMBL4104861',
      'CHEMBL1502197',
      'CHEMBL3931303',
      'CHEMBL466111',
      'CHEMBL323959',
      'CHEMBL1255073',
      'CHEMBL4288214',
      'CHEMBL3480964',
    ],
  },
  f2: {
    t2: [
      'CHEMBL2363046',
      'CHEMBL2363048',
      'CHEMBL615043',
      'CHEMBL2363054',
      'CHEMBL5693',
      'CHEMBL2366687',
      'CHEMBL2079',
      'CHEMBL6103',
      'CHEMBL2364032',
      'CHEMBL2364036',
    ],
  },
}

const possibleSeparators = {
  Automatic: {
    id: 'Auto',
    name: 'Automatic',
    character: ',',
    submissionCharacter: '__AUTO__',
  },
  Comma: {
    id: 'Comma',
    name: 'Comma (,)',
    character: ',',
  },
  Colon: {
    id: 'Colon',
    name: 'Colon (:)',
    character: ':',
  },
  Semicolon: {
    id: 'Semicolon',
    name: 'Semicolon (;)',
    character: ';',
  },
  Space: {
    id: 'Space',
    name: 'Space ( )',
    character: ' ',
  },
  NewLine: {
    id: 'NewLine',
    name: 'New Line (\\n)',
    character: '\n',
    submissionCharacter: '__NEW_LINE__',
  },
}

const possibleModes = {
  PastedIDs: 'PASTED_IDS',
  UploadedFile: 'UPLOADED_FILE',
}

const helpers = {
  setReadyToSearch(state) {
    if (state.searchMode === possibleModes.PastedIDs) {
      if (state.numItemsIdentified > 0) {
        state.whatIsMissingToSearchMsg = ''
        state.readyToSearch = true
      } else {
        state.whatIsMissingToSearchMsg =
          'Please paste some ids to activate the search button'
        state.readyToSearch = false
      }
    } else if (state.searchMode === possibleModes.UploadedFile) {
      if (state.idsFile != null) {
        state.whatIsMissingToSearchMsg = ''
        state.readyToSearch = true
      } else {
        state.whatIsMissingToSearchMsg =
          'Please select a file to activate the search button'
        state.readyToSearch = false
      }
    }
  },
  generateUploadProgressFunction(commit) {
    return (progressEvent) => {
      const percentCompleted = Math.round(
        (progressEvent.loaded * 100) / progressEvent.total
      )
      commit('SET_SUBMISSION_PROGRESS', percentCompleted)
    }
  },
  redirectToResultsPage(searchJobID) {
    const searchResultsUrl = process.env.searchByIDsResultsURLTemplate.replace(
      '<JOB_ID>',
      searchJobID
    )

    window.top.location.href = searchResultsUrl
  },
}

export const state = () => ({
  from: possibleFroms.f1,
  fromItems: _.values(possibleFroms),
  toItems: [possibleTos.t1],
  to: possibleTos.t1,
  pastedIDs: '',
  usingSample: false,
  separator: possibleSeparators.Automatic,
  separators: _.values(possibleSeparators),
  automaticSeparatorLoaded: undefined,
  identifiedSeparatorCharacter: 'SEP',
  numItemsIdentified: 0,
  readyToSearch: false,
  whatIsMissingToSearchMsg: '',
  idsFile: undefined,
  searchMode: possibleModes.PastedIDs,
  submissionProgress: -1,
})

export const mutations = {
  SET_FROM(state, newFrom) {
    state.from = newFrom
  },
  UPDATE_TO_ITEMS(state) {
    const tosIDs = fromToMap[state.from.id]
    const tosList = []

    for (let i = 0; i < tosIDs.length; i++) {
      const currentID = tosIDs[i]
      tosList.push(possibleTos[currentID])
    }

    state.toItems = tosList
    state.to = tosList[0]
  },
  UPDATE_PASTED_IDS(state, newText) {
    state.pastedIDs = newText
    state.usingSample = false
  },
  SET_SAMPLE_ITEMS(state) {
    const currentFromID = state.from.id
    const currentToID = state.to.id

    const sampleIDs = examplesMap[currentFromID][currentToID]
    state.pastedIDs = sampleIDs.join(state.separator.character)
    state.usingSample = true
  },
  SET_SEPARATOR(state, newSeparator) {
    state.separator = newSeparator
  },
  SET_AUTOMATIC_SEPARATOR_LOADED(state, isLoaded) {
    state.automaticSeparatorLoaded = isLoaded
  },
  SET_AUTOMATIC_SEPARATOR_IDENTIFIED(state, identifiedSeparatorCharacter) {
    state.identifiedSeparatorCharacter = identifiedSeparatorCharacter
  },
  UPDATE_IDENTIFIED_ITEMS_COUNT(state) {
    let iDsText = ''
    if (state.pastedIDs != null) {
      // Sanitise the text first
      const currentSeparator = state.separator.character
      iDsText = state.pastedIDs.replace(
        RegExp(`${currentSeparator}{2,}`, 'g'),
        ','
      )
      iDsText = iDsText.replace(RegExp(`${currentSeparator}$`), '')
    }
    if (iDsText === '') {
      state.numItemsIdentified = 0
    } else {
      state.numItemsIdentified = iDsText.split(state.separator.character).length
    }
    helpers.setReadyToSearch(state)
  },
  RESET_IDENTIFIED_ITEMS_COUNT(state) {
    state.numItemsIdentified = 0
  },
  SET_IDS_FILE(state, idsFile) {
    state.idsFile = idsFile
    helpers.setReadyToSearch(state)
  },
  SELECT_SEARCH_MODE(state, searchMode) {
    state.searchMode = searchMode
    helpers.setReadyToSearch(state)
  },
  SET_SUBMISSION_PROGRESS(state, currentProgress) {
    state.submissionProgress = currentProgress
  },
}

export const actions = {
  updateToItems({ commit, state }, newFrom) {
    commit('SET_FROM', newFrom)
    commit('UPDATE_TO_ITEMS')
    if (state.usingSample) {
      // If was using sample, delete text to avoid confusions by the User.
      commit('UPDATE_PASTED_IDS', '')
    }
  },
  identifySeparator({ commit, state, dispatch }) {
    commit('SET_AUTOMATIC_SEPARATOR_LOADED', false)
    SubmissionService.identifySeparator(state.pastedIDs, state.from.value)
      .then((response) => {
        commit('SET_AUTOMATIC_SEPARATOR_IDENTIFIED', response.data.separator)
        commit('UPDATE_IDENTIFIED_ITEMS_COUNT')
        commit('SET_AUTOMATIC_SEPARATOR_LOADED', true)
      })
      .catch((error) => {
        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          'There was a problem identifying the separator'
        )
      })
  },
  updatePastedIDs({ commit, state, dispatch }, newText) {
    commit('UPDATE_PASTED_IDS', newText)

    if (state.separator.id === 'Auto') {
      commit('RESET_IDENTIFIED_ITEMS_COUNT')
      dispatch('identifySeparator')
    } else {
      commit('UPDATE_IDENTIFIED_ITEMS_COUNT')
    }
  },
  setSampleItems({ commit, state }) {
    if (state.separator.id === 'Auto') {
      commit('SET_AUTOMATIC_SEPARATOR_IDENTIFIED', ',')
      commit('SET_AUTOMATIC_SEPARATOR_LOADED', true)
    }
    commit('SET_SAMPLE_ITEMS')
    commit('UPDATE_IDENTIFIED_ITEMS_COUNT')
  },
  setSeparator({ commit, state, dispatch }, newSeparator) {
    commit('SET_SEPARATOR', newSeparator)
    if (state.usingSample) {
      commit('SET_SAMPLE_ITEMS')
    }

    if (state.separator.id === 'Auto') {
      dispatch('identifySeparator')
    }
  },
  setIDsFile({ commit }, idsFile) {
    commit('SET_IDS_FILE', idsFile)
  },
  selectSearchMode({ commit }, searchMode) {
    commit('SELECT_SEARCH_MODE', searchMode)
  },
  submitSearch({ commit, state, dispatch }) {
    const separatorCharacter =
      state.separator.submissionCharacter || state.separator.character
    const searchParams = {
      from: state.from.value,
      to: state.to.value,
      separator: separatorCharacter,
      dl__ignore_cache: process.env.delayedJobsIgnoreCache,
    }
    commit('SET_SUBMISSION_PROGRESS', 0)
    let submissionPromise

    if (state.searchMode === possibleModes.PastedIDs) {
      searchParams.raw_items_ids = state.pastedIDs
      submissionPromise = SubmissionService.submitSearchFromRawIDs(
        searchParams,
        helpers.generateUploadProgressFunction(commit)
      )
    } else {
      searchParams.input1 = state.idsFile
      submissionPromise = SubmissionService.submitSearchFromFile(
        searchParams,
        helpers.generateUploadProgressFunction(commit)
      )
    }
    submissionPromise
      .then((response) => {
        commit('SET_SUBMISSION_PROGRESS', -1)
        const jobID = response.data.job_id
        const detailsLink = {
          text: 'See Status',
          url: Common.getSearchJobStatusUrl(jobID),
        }
        RequestNotifications.dispatchRequestSuccessNotification(
          dispatch,
          'Job submitted succesfully!',
          detailsLink
        )
        window.parent.postMessage(
          'SEARCH_BY_IDS_SUBMITTED_SUCCESSFULLY',
          process.env.iframeTargetOrigin
        )
        helpers.redirectToResultsPage(jobID)
      })
      .catch((error) => {
        commit('SET_SUBMISSION_PROGRESS', -1)
        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          'There was a problem submitting the search: '
        )
      })
  },
}
