import axios from 'axios'

const esProxyApiClient = axios.create({
  baseURL: process.env.esProxyBaseUrl,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
  },
})

export { esProxyApiClient }
