export const state = () => ({
  isEmbedded: false,
})

export const mutations = {
  SET_IS_EMBEDDED(state, isEmbedded) {
    state.isEmbedded = isEmbedded
  },
}

export const actions = {
  setIsEmbedded({ commit }, isEmbedded = true) {
    commit('SET_IS_EMBEDDED', isEmbedded)
  },
}
