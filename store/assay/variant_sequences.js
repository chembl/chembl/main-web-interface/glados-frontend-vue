import IndexNames from '@/web-components-submodule/standardisation/IndexNames.js'
import ESProxyService from '@/web-components-submodule/services/ESProxyService.js'
import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'

export const state = () => ({
  variantSecuenceInfo: {},
  dataLoaded: false,
})

const methods = {
  getQuery(chemblID) {
    return {
      _source: ['variant_sequence'],
      query: {
        terms: {
          _id: [chemblID],
        },
      },
    }
  },
}

export const mutations = {
  SET_DATA_LOADED(state, dataLoaded) {
    state.dataLoaded = dataLoaded
  },
  SET_VARIANT_SEQUENCE_INFO(state, variantSecuenceInfo) {
    state.variantSecuenceInfo = variantSecuenceInfo
  },
}

export const actions = {
  loadData({ commit, state, dispatch }, chemblID) {
    const indexName = IndexNames.getAssayIndexName()
    const query = methods.getQuery(chemblID)
    ESProxyService.getESData(indexName, query)
      .then((response) => {
        const hits = response.data.es_response.hits.hits
        if (hits.length > 0) {
          const variantSecuenceInfo = hits[0]._source.variant_sequence
          commit('SET_VARIANT_SEQUENCE_INFO', variantSecuenceInfo)
          commit('SET_DATA_LOADED', true)
        }
      })
      .catch((error) => {
        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          `There was an error while loading the variant sequence information! `
        )
      })
  },
}
