import Vue from 'vue'

export const state = () => ({
  pastedSequence: undefined,
  readyToSearch: false,
  optionalParams: {},
})

export const mutations = {
  UPDATE_PASTED_SEQUENCE(state, pastedSequence) {
    state.pastedSequence = pastedSequence
  },
  SET_READY_TO_SEARCH(state, readyToSearch) {
    state.readyToSearch = readyToSearch
  },
  SET_OPTIONAL_PARAM(state, { paramID, selectedValue }) {
    Vue.set(state.optionalParams, paramID, selectedValue)
  },
}

export const actions = {
  updatePastedSequence({ commit, state, dispatch }, pastedSequence) {
    commit('UPDATE_PASTED_SEQUENCE', pastedSequence)
    const readyToSearch = pastedSequence != null && pastedSequence !== ''
    commit('SET_READY_TO_SEARCH', readyToSearch)
  },
  updateOptionalParam({ commit, state, dispatch }, { paramID, selectedValue }) {
    commit('SET_OPTIONAL_PARAM', { paramID, selectedValue })
  },
}
