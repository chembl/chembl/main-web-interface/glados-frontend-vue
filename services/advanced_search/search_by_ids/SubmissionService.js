import { djApiClient } from '~/web-components-submodule/api_clients/DelajedJobsAPIClient.js'
import { esProxyApiClient } from '~/web-components-submodule/api_clients/ESProxyAPIClient.js'

export default {
  submitSearchFromRawIDs(searchParams, progressFunction) {
    const formData = new FormData()
    formData.append('from', searchParams.from)
    formData.append('to', searchParams.to)
    formData.append('separator', searchParams.separator)
    formData.append('dl__ignore_cache', searchParams.dl__ignore_cache)
    formData.append('raw_items_ids', searchParams.raw_items_ids)

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
      onUploadProgress: progressFunction,
    }

    return djApiClient.post('/submit/search_by_ids_job', formData, config)
  },
  submitSearchFromFile(searchParams, progressFunction) {
    const formData = new FormData()
    formData.append('from', searchParams.from)
    formData.append('to', searchParams.to)
    formData.append('separator', searchParams.separator)
    formData.append('dl__ignore_cache', searchParams.dl__ignore_cache)
    formData.append('input1', searchParams.input1)

    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      onUploadProgress: progressFunction,
    }
    return djApiClient.post('/submit/search_by_ids_job', formData, config)
  },
  identifySeparator(pastedIDs, fromValue) {
    const formData = new FormData()
    formData.append('text', pastedIDs)
    formData.append('type_of_ids', fromValue)
    return esProxyApiClient.post('/utils/identify_separator', formData)
  },
}
