<template>
  <div class="main-container">
    <resize-observer v-if="isEmbedded" @notify="handleResize" />

    <v-card flat>
      <v-card-text v-if="showError" class="text-caption error--text">
        Error: {{ errorMsg }}.
      </v-card-text>
      <v-skeleton-loader v-if="loading" type="card"></v-skeleton-loader>
      <v-card-text v-else-if="noData">
        <v-card flat outlined>
          <v-card-title
            >No Data Available. (ChEMBL Ligand Efficiency Plot for Target
            {{ idText }})</v-card-title
          >
        </v-card>
      </v-card-text>

      <v-card-text v-else>
        <v-card flat outlined>
          <v-card-text>
            <a :href="linkToActivities" target="_parent">
              See all activities used in this plot ({{ numDataPoints }})
            </a>
            <div v-if="thereIsMoreData">
              <v-icon> mdi-alert </v-icon>
              (Showing first
              {{ maxItemsInPlot }} data points out of {{ numDataPoints }})
            </div>
            <br />
            <v-divider />
            <br />
            <ScatterPlot :scatter-plot-config="scatterPlotConfig" />
          </v-card-text>
          <v-card-text class="text-caption">
            The Ligand Efficiency chart plots Binding Efficiency Index (BEI)
            against Surface Efficiency Index (SEI), where:
            <br />
            <ul>
              <li>SEI = (-log10(Standard Value*10^-9))*100/PSA</li>
              <li>BEI = (-log10(Standard Value*10^-9))*1000/MWT</li>
            </ul>
          </v-card-text>
        </v-card>
      </v-card-text>
    </v-card>
  </div>
</template>

<script>
import { resizeMessagesMixin } from '~/web-components-submodule/mixins/embedding/resizeMessages.js'
import { embeddingStateMixin } from '~/web-components-submodule/mixins/embedding/embeddingState.js'
import IndexNames from '~/web-components-submodule/standardisation/IndexNames.js'
import ESProxyService from '~/web-components-submodule/services/ESProxyService.js'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import LinksToBrowsers from '~/web-components-submodule/standardisation/LinksToBrowsers.js'
import ScatterPlot from '~/components/report_cards/shared/ScatterPlot/ScatterPlot.vue'
import LinksToEntities from '~/web-components-submodule/standardisation/LinksToEntities.js'

export default {
  components: {
    ScatterPlot,
  },
  mixins: [resizeMessagesMixin, embeddingStateMixin],
  props: {
    chemblID: {
      type: String,
      default: () => undefined,
    },
  },
  data() {
    return {
      loading: true,
      showError: false,
      errorMsg: undefined,
      prefName: undefined,
      numDataPoints: 0,
      maxItemsInPlot: 1000,
      dataPoints: [],
    }
  },
  computed: {
    idText() {
      return this.prefName != null
        ? `${this.chemblID} (${this.prefName})`
        : this.chemblID
    },
    activitiesQuerystring() {
      return `target_chembl_id:${this.chemblID} AND standard_type:(IC50 OR Ki OR EC50 OR Kd) AND _exists_:standard_value AND _exists_:ligand_efficiency`
    },
    datasetDescriptionText() {
      return `Activities used in ligand efficiencies plot for Target ${this.idText}`
    },
    linkToActivities() {
      return LinksToBrowsers.buildURLForEntityBrowser(
        EntityNames.Activity.entityID,
        this.activitiesQuerystring,
        'querystring',
        this.datasetDescriptionText
      )
    },
    noData() {
      return this.numDataPoints === 0
    },
    thereIsMoreData() {
      return this.numDataPoints > this.maxItemsInPlot
    },
    scatterPlotConfig() {
      return {
        title: `Ligand Efficiencies for Target ${this.idText}`,
        yAxisProperty: 'ligand_efficiency.bei',
        yAxisLabel: 'Binding Efficiency Index (BEI)',
        xAxisProperty: 'ligand_efficiency.sei',
        xAxisLabel: 'Surface Efficiency Index (SEI)',
        zAxisProperty: 'standard_value',
        zAxisLabel: 'Standard Value nM',
        idLabel: 'Molecule',
        dataPoints: this.dataPoints,
        legend: {
          thresholds: [
            {
              max: 1,
              color: '#00695C',
            },
            {
              max: 100,
              color: '#00897B',
            },
            {
              max: 1000,
              color: '#E0F2F1',
            },
            {
              color: '#EF5350',
            },
          ],
        },
      }
    },
  },
  async mounted() {
    try {
      const targetIndexName = IndexNames.getIndexNameFromEntityID(
        EntityNames.Target.entityID
      )
      const targetDocSource = ['pref_name']
      const targetDocResponse = await ESProxyService.getESDocument(
        targetIndexName,
        this.chemblID,
        targetDocSource
      )

      const sourceObtained = targetDocResponse.data._source
      this.prefName = sourceObtained.pref_name

      const activityIndexName = IndexNames.getIndexNameFromEntityID(
        EntityNames.Activity.entityID
      )

      const activityQuery = {
        size: this.maxItemsInPlot,
        track_total_hits: true,
        _source: [
          'molecule_chembl_id',
          'ligand_efficiency',
          'standard_value',
          'standard_relation',
          'standard_type',
        ],
        query: {
          query_string: {
            query: this.activitiesQuerystring,
          },
        },
      }

      const activityDataResponse = await ESProxyService.getESData(
        activityIndexName,
        activityQuery
      )
      const activityData = activityDataResponse.data.es_response
      this.numDataPoints = activityData.hits.total.value
      if (this.numDataPoints === 0) {
        this.loading = false
        return
      }

      const prefNamesQuery = {
        size: this.numDataPoints,
        _source: ['molecule_chembl_id', 'pref_name'],
        query: {
          terms: {
            molecule_chembl_id: activityData.hits.hits.map(
              (hit) => hit._source.molecule_chembl_id
            ),
          },
        },
      }
      const prefNamesResponse = await ESProxyService.getESData(
        IndexNames.getIndexNameFromEntityID(EntityNames.Compound.entityID),
        prefNamesQuery
      )
      const prefNamesData = prefNamesResponse.data.es_response.hits.hits
      // map molecule_chembl_id to pref_name
      const prefNamesMap = prefNamesData.reduce((acc, hit) => {
        acc[hit._source.molecule_chembl_id] = hit._source.pref_name
        return acc
      }, {})

      this.dataPoints = activityData.hits.hits.map((hit) => {
        const prefName = prefNamesMap[hit._source.molecule_chembl_id]
        const moleculeChEMBLID = hit._source.molecule_chembl_id

        const linkToReportCard = LinksToEntities[
          EntityNames.Compound.entityID
        ].getLinkToReportCard(moleculeChEMBLID)

        return {
          ...hit._source,
          idText:
            prefName != null
              ? `${moleculeChEMBLID} (${prefName})`
              : moleculeChEMBLID,
          linkToReportCard,
        }
      })

      this.loading = false
    } catch (error) {
      this.loading = false
      this.showError = true
      this.errorMsg = error
    }
  },
}
</script>

<style scoped lang="scss">
.main-container {
  position: relative;
}
</style>
