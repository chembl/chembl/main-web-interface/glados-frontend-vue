import IndexNames from '@/web-components-submodule/standardisation/IndexNames.js'
import ESProxyService from '@/web-components-submodule/services/ESProxyService.js'
import RequestNotifications from '@/web-components-submodule/utils/RequestNotifications.js'
import LinksToEntities from '@/web-components-submodule/standardisation/LinksToEntities.js'

export const state = () => ({
  dataLoaded: false,
  entityExists: false,
  idStatus: undefined,
  idStatusDescription: undefined,
  entityType: undefined,
  linkToEntity: undefined,
  isActive: false,
  lastActiveIsKnown: false,
  lastReleaseActive: undefined,
  apiURL: undefined,
})

const methods = {
  getQuery(chemblID) {
    return {
      query: {
        terms: {
          _id: [chemblID],
        },
      },
    }
  },
  getIDStatusDescription(idStatus) {
    if (idStatus === 'ACTIVE') {
      return 'The ID is active in the ChEMBL Database'
    }
    if (idStatus === 'OBS') {
      return 'The ID has been permanently deleted from ChEMBL.'
    }
    if (idStatus === 'INACTIVE') {
      return 'The ID has not been included in the latest release.'
    }
  },
}

export const mutations = {
  SET_DATA_LOADED(state, dataLoaded) {
    state.dataLoaded = dataLoaded
  },
  SET_ENTITY_EXISTS(state, entityExists) {
    state.entityExists = entityExists
  },
  SET_ID_STATUS(state, idStatus) {
    state.idStatus = idStatus
  },
  SET_ID_STATUS_DESCRIPTION(state, idStatusDescription) {
    state.idStatusDescription = idStatusDescription
  },
  SET_ENTITY_TYPE(state, entityType) {
    state.entityType = entityType
  },
  SET_LINK_TO_ENTITY(state, linkToEntity) {
    state.linkToEntity = linkToEntity
  },
  SET_IS_ACTIVE(state, isActive) {
    state.isActive = isActive
  },
  SET_LAST_ACTIVE_IS_KNOWN(state, lastActiveIsKnown) {
    state.lastActiveIsKnown = lastActiveIsKnown
  },
  SET_LAST_RELEASE_ACTIVE(state, lastReleaseActive) {
    state.lastReleaseActive = lastReleaseActive
  },
  SET_API_URL(state, apiURL) {
    state.apiURL = apiURL
  },
}

export const actions = {
  loadData({ commit, state, dispatch }, chemblID) {
    const indexName = IndexNames.getIDLookupIndexName()
    const query = methods.getQuery(chemblID)
    ESProxyService.getESData(indexName, query)
      .then((response) => {
        const numResults = response.data.es_response.hits.total.value
        if (numResults === 0) {
          commit('SET_ENTITY_EXISTS', false)
        } else {
          const entityData = response.data.es_response.hits.hits[0]._source
          const idStatus = entityData.status
          commit('SET_ID_STATUS', idStatus)
          const idStatusDescription = methods.getIDStatusDescription(idStatus)
          commit('SET_ID_STATUS_DESCRIPTION', idStatusDescription)
          const entityType = entityData.entity_type
          commit('SET_ENTITY_TYPE', entityType)
          const lastActive = entityData.last_active
          commit('SET_LAST_RELEASE_ACTIVE', lastActive)
          const lastActiveIsKnown = lastActive != null
          commit('SET_LAST_ACTIVE_IS_KNOWN', lastActiveIsKnown)
          const apiURL = `${process.env.wsLookupBaseUrl}${entityData.resource_url}`
          commit('SET_API_URL', apiURL)

          if (idStatus === 'ACTIVE') {
            const officialIndexName = IndexNames.getIndexNameFromLookupEntityName(
              entityType
            )
            const linkToEntity = LinksToEntities[
              officialIndexName
            ].getLinkToReportCard(chemblID)
            commit('SET_IS_ACTIVE', true)
            commit('SET_LINK_TO_ENTITY', linkToEntity)
          }

          commit('SET_ENTITY_EXISTS', true)
        }

        commit('SET_DATA_LOADED', true)
      })
      .catch((error) => {
        RequestNotifications.dispatchRequestErrorNotification(
          error,
          dispatch,
          `There was an error while loading the variant sequence information! `
        )
      })
  },
}
